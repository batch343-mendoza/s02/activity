-- CREATE blog_db DATABASE
CREATE DATABASE blog_db;

-- USE the blog_db Database
USE blog_db;

-- TABLES --

-- Users Table

CREATE TABLE users(
	id INT NOT NULL AUTO_Increment,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

-- posts Table
CREATE TABLE posts(
	id INT NOT NULL AUTO_Increment,
	title VARCHAR(500) NOT NULL,
	content VARCHAR(5000) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	author_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_author_id
		FOREIGN KEY (author_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- post_likes table
CREATE TABLE post_likes(
	id INT NOT NULL AUTO_Increment,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	datetime_liked DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_likes_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_likes_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- post_commonets table
CREATE TABLE post_comments(
	id INT NOT NULL AUTO_Increment,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	content VARCHAR(5000) NOT NULL,
	datetime_commented DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_comments_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_comments_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- to see tables in blog_db use this syntax to the sql query or in the MariaDB 
SHOW TABLES;